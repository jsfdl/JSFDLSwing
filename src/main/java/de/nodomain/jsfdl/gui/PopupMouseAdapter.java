package de.nodomain.jsfdl.gui;


import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadsMvp;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PopupMouseAdapter extends MouseAdapter {

    private final JTable jtable;
    private final JTablePopupMenu popup;

    public PopupMouseAdapter(JTable jtable, DownloadsMvp.View view) {
        this.jtable = jtable;
        this.popup = new JTablePopupMenu(jtable, view);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // Right mouse click
        if (SwingUtilities.isRightMouseButton(e)) {
            // get the coordinates of the mouse click
            Point p = e.getPoint();

            // get the row index that contains that coordinate
            int rowNumber = jtable.rowAtPoint(p);

            // Get the ListSelectionModel of the JTable
            ListSelectionModel model = jtable.getSelectionModel();

            // set the selected interval of rows. Using the "rowNumber"
            // variable for the beginning and end selects only that one row.
            model.setSelectionInterval(rowNumber, rowNumber);

            popup.show(jtable, (int) p.getX(), (int) p.getY());
        }
    }
}

