package de.nodomain.jsfdl.gui;


import com.gitlab.jsfdl.jsfdl.bus.events.downloadchanged.DownloadInfoBuilderForSFDL;
import com.gitlab.jsfdl.jsfdl.bus.events.downloadchanged.DownloadInformation;
import com.gitlab.jsfdl.jsfdl.bus.events.sfdladded.SFDLFileAddedInformation;
import com.gitlab.jsfdl.jsfdl.ftp.State;
import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadsMvp;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SFDLTableModel extends AbstractTableModel implements DownloadsMvp.DownloadChangedListener {

    private final List<DownloadInformation> data = new ArrayList<>();
    private final static int COLUMN_COUNT = 5;

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMN_COUNT;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Dateiname";
            case 1:
                return "Dateigröße";
            case 2:
                return "Fortschritt";
            case 3:
                return "Status";
            case 4:
                return "Geschwindigkeit";
            default:
                throw new RuntimeException("Illegal state: requested " + columnIndex);
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        if (data.isEmpty()) {
            System.out.println("wtf2);");
        }

        switch (columnIndex) {
            case 0:
                return data.get(rowIndex).getSfdlEntry().getDisplayName();

            case 1:
                return String.format("%.02f MB.", data.get(rowIndex).getSizeOfPackageInBytes() / 1024f / 1024f);
            case 2:
                return String.format("%.02f MB.", data.get(rowIndex).getTransferedBytes() / 1024f / 1024f);
            case 3:
                if (null == data.get(rowIndex).getState()) {
                    return "Waiting";
                } else {
                    return data.get(rowIndex).getState().stringRepres();
                }
            case 4:
                return data.get(rowIndex).getBytePerSecond() / 1024 + " kb/s";
        }

        return "n/a";
    }

    public void removeSfdl(int id) {
        Iterator<DownloadInformation> iterator = data.iterator();
        while (iterator.hasNext()) {
            DownloadInformation next = iterator.next();
            if (next.getId() == id) {
                iterator.remove();
                fireTableRowsDeleted(data.indexOf(next), data.indexOf(next));
            }

        }
    }

    public void addSfdlToDownload(SFDLFileAddedInformation payload) {
        //TODO: converter
        DownloadInformation build = new DownloadInfoBuilderForSFDL(payload.getSfdlEntry()).build();
        data.add(build);
        fireTableRowsInserted(getRowCount() - 1, getRowCount() - 1);
    }

    @Override
    public void downloadChanged(DownloadInformation info) {
        for (DownloadInformation info1 : data) {
            if (info1.getId() == info.getId()) {
                int index = data.indexOf(info1);
                if (index == -1) {
                    return;
                }
                data.set(index, new DownloadInfoBuilderForSFDL(info).build());
                fireTableRowsUpdated(index, index);
            }
        }
    }

    public void allDownloadsStarted(List<Integer> started) {
        data.stream().filter(started::contains).forEach((t) -> {
            int index = data.indexOf(t);
            data.set(index, new DownloadInfoBuilderForSFDL(t).inState(State.DOWNLOADING).build());
            fireTableRowsUpdated(index, index);
        });
    }

    public void allDownloadsAborted(List<Integer> aborted) {
        data.stream().filter(aborted::contains).forEach((t) -> {
            int index = data.indexOf(t);
            data.set(index, new DownloadInfoBuilderForSFDL(t).inState(State.ABORTED).build());
            fireTableRowsUpdated(index, index);
        });
    }

    public int getSFDLIdAtRow(int selectedRow) {
        return data.get(selectedRow).getId();
    }

//    public void stopSfdl(int sfdlIdAtRow) {
//        data.get(1).
//    }
}
