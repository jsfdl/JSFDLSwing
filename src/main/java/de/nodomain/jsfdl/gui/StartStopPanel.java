package de.nodomain.jsfdl.gui;

import com.gitlab.jsfdl.jsfdl.mvp.startstop.StartStopMvp;
import com.gitlab.jsfdl.jsfdl.mvp.startstop.StartStopView;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@StartStopView
public final class StartStopPanel extends JPanel implements StartStopMvp.View {

    private final List<StartStopMvp.StartDownloadListener> startDownloadListeners = new ArrayList<>();
    private final List<StartStopMvp.CancelDownloadListener> cancelDownloadListeners = new ArrayList<>();

    public StartStopPanel() {
        setBorder(new LineBorder(Color.black));
        JButton jButton1 = new javax.swing.JButton();
        JButton jButton2 = new javax.swing.JButton();
        jButton1.setText("Alle Downloads starten");
        jButton1.addActionListener((java.awt.event.ActionEvent evt) -> {
            startDownloadListeners.stream().forEach((listener) -> {
                listener.downloadsStarted();
            });
        });

        jButton2.setText("Alle Downloads abbrechen");
        jButton2.addActionListener((java.awt.event.ActionEvent evt) -> {
            cancelDownloadListeners.stream().forEach((listener) -> {
                listener.downloadsCanceled();
            });
        });
        GroupLayout jPanel1Layout = new GroupLayout(this);
        setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jButton1)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jButton2)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton1)
                        .addComponent(jButton2))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

    }

    @Override
    public void registerStartDownloadListener(StartStopMvp.StartDownloadListener actionListener) {
        startDownloadListeners.add(actionListener);
    }

    @Override
    public void registerCancelDownloadListener(StartStopMvp.CancelDownloadListener actionListener) {
        cancelDownloadListeners.add(actionListener);
    }

    @Override
    public void viewDownloadsAreStopped() {
        JOptionPane.showMessageDialog(this, "Downloads stopped!");
    }

    @Override
    public void viewDownloadsAreStarted() {
        JOptionPane.showMessageDialog(this, "Downloads started!");
    }

    @Override
    public void showNothingToAbort() {
        JOptionPane.showMessageDialog(this, "Nothing to abort!");
    }

    @Override
    public void viewNoDownloadsToDownloads() {
        JOptionPane.showMessageDialog(this, "Nothing to download!");
    }
}
