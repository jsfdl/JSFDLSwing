package de.nodomain.jsfdl.gui;


import com.gitlab.jsfdl.jsfdl.mvp.dateiMenu.DateiMenuMvp;
import com.gitlab.jsfdl.jsfdl.mvp.dateiMenu.DateiMenuView;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.io.File;
import java.net.MalformedURLException;
import java.util.*;

import static javax.swing.JOptionPane.ERROR_MESSAGE;

@DateiMenuView
public final class MenuBar extends JMenuBar implements DateiMenuMvp.View {

    private final JMenu jMenu1 = new JMenu();
    private final JMenu jMenu2 = new JMenu();
    private final JMenu jMenu3 = new JMenu();
    private final JMenu jMenu4 = new JMenu();
    private final JMenu jMenu5 = new JMenu();
    private final JMenuBar jMenuBar1 = new JMenuBar();
    private final JMenuItem jMenuItem1 = new JMenuItem();
    private final JMenuItem jMenuItem10 = new JMenuItem();
    private final JMenuItem jMenuItem11 = new JMenuItem();
    private final JMenuItem jMenuItem12 = new JMenuItem();
    private final JMenuItem jMenuItem13 = new JMenuItem();
    private final JMenuItem jMenuItem14 = new JMenuItem();
    private final JMenuItem jMenuItem15 = new JMenuItem();
    private final JMenuItem jMenuItem16 = new JMenuItem();
    private final JMenuItem jMenuItem2 = new JMenuItem();
    private final JMenuItem jMenuItem3 = new JMenuItem();
    private final JMenuItem jMenuItem4 = new JMenuItem();
    private final JMenuItem jMenuItem5 = new JMenuItem();
    private final JMenuItem jMenuItem6 = new JMenuItem();
    private final JMenuItem jMenuItem7 = new JMenuItem();
    private final JMenuItem jMenuItem8 = new JMenuItem();
    private final JMenuItem jMenuItem9 = new JMenuItem();
    private final JPopupMenu.Separator jSeparator1 = new JPopupMenu.Separator();
    private final JPopupMenu.Separator jSeparator2 = new JPopupMenu.Separator();

    private final List<DateiMenuMvp.SFDLFileRemovedListener> listeners = new ArrayList<>();

    public MenuBar() {
        jMenu1.setText("Datei");

        jMenuItem1.setText("Öffnen");
        jMenuItem1.addActionListener(this::showSFDLSelectFileDialog);
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Einstellungen");
        jMenuItem2.setEnabled(false);
        jMenu1.add(jMenuItem2);

        jMenuItem3.setText("Beenden");
        jMenuItem3.addActionListener(e -> System.exit(0));
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Bearbeiten");

        jMenuItem4.setText("Alle fertigen Downloads entfernen");
        jMenuItem4.setEnabled(false);
        jMenu2.add(jMenuItem4);

        jMenuItem5.setText("Alle Downloads entfernen");
        jMenuItem5.setEnabled(false);
        jMenu2.add(jMenuItem5);

        jMenuItem6.setText("SFDL Datei schließen");
        jMenuItem6.setEnabled(false);
        jMenu2.add(jMenuItem6);
        jMenu2.add(jSeparator1);

        jMenuItem7.setText("Alle Dateien markieren");
        jMenuItem7.setEnabled(false);
        jMenu2.add(jMenuItem7);

        jMenuItem8.setText("Alle Dateien demarkieren");
        jMenuItem8.setEnabled(false);
        jMenu2.add(jMenuItem8);
        jMenu2.add(jSeparator2);

        jMenuItem9.setText("Alle Pakete einklappen");
        jMenuItem9.setEnabled(false);
        jMenu2.add(jMenuItem9);

        jMenuItem10.setText("Alle Pakete erweitern");
        jMenuItem10.setEnabled(false);
        jMenu2.add(jMenuItem10);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Extras");

        jMenu5.setText("Nach Download");

        jMenuItem12.setText("SFDL Loader schließen");
        jMenuItem12.setEnabled(false);
        jMenu5.add(jMenuItem12);


        jMenu3.add(jMenu5);

        jMenuItem11.setText("Plugin Manager");
        jMenuItem11.setEnabled(false);
        jMenu3.add(jMenuItem11);

        jMenuBar1.add(jMenu3);

        jMenu4.setText("Hilfe");

        jMenuItem14.setText("Über JSFDL");
        jMenuItem14.setEnabled(false);
        jMenu4.add(jMenuItem14);

        jMenuItem15.setText("Einstellungen zurücksetzen");
        jMenuItem15.setEnabled(false);
        jMenu4.add(jMenuItem15);

        jMenuItem16.setText("Log anzeigen");
        jMenuItem16.setEnabled(false);
        jMenu4.add(jMenuItem16);

        jMenuBar1.add(jMenu4);

        add(jMenuBar1);

    }
    private final JFileChooser fileChooser = new JFileChooser();
    private final FileNameExtensionFilter filter = new FileNameExtensionFilter("SFDLs", "sfdl");

    public void showSFDLSelectFileDialog(ActionEvent event) {
        fileChooser.setFileFilter(filter);
        fileChooser.setMultiSelectionEnabled(true);
        final int returnVal = fileChooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File[] selectedFiles = fileChooser.getSelectedFiles();
            Arrays.stream(selectedFiles).forEach((t) -> {
                sfdlFileSelectedListeners.forEach((listener) -> {
                  try {
                    listener.sfdlFileSelected(t.toURI().toURL());
                  } catch (MalformedURLException ex) {
                    throw new RuntimeException(ex);
                  }
                });
            });

        }
    }
    private final Set<DateiMenuMvp.SFDLFileSelectionDialogListener> sfdlFileSelectionDialogListeners = new HashSet<>();

    private final Set<DateiMenuMvp.SFDLFileSelectedListener> sfdlFileSelectedListeners = new HashSet<>();

    @Override
    public void registerSFDLFileSelectedListener(DateiMenuMvp.SFDLFileSelectedListener listener) {
        sfdlFileSelectedListeners.add(listener);
    }

    @Override
    public void showSFDLAddedDialog(String sfdlFile) {
//        JOptionPane.showMessageDialog(this, "The SFDL " + sfdlFile.getDescription() + " has been added!");
    }

    @Override
    public void showSFDLSelectFileDialog() {
        showSFDLSelectFileDialog(null); //todo: very ugly
    }

    @Override
    public Optional<String> requestPasswordDialog() {
        PasswordEnterDialog passwordEnterDialog = new PasswordEnterDialog();
        passwordEnterDialog.setVisible(true);
        if (passwordEnterDialog.isPasswordEntered()) {
            return Optional.of(passwordEnterDialog.getPassword());
        } else {
            return Optional.ofNullable(null);
        }
    }

    @Override
    public void showWrongPasswordEntered() {
        JOptionPane.showMessageDialog(this, "Wrong password entered?", "Unable to decrypt SFDL", ERROR_MESSAGE);
    }

    @Override
    public void registerSFDLFileRemoveListener(DateiMenuMvp.SFDLFileRemovedListener listener) {
        listeners.add(listener);
    }

}
