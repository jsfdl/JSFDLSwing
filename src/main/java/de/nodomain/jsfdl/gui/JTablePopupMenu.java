
package de.nodomain.jsfdl.gui;

import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadsMvp;

import javax.swing.*;
import java.awt.event.ActionEvent;

public final class JTablePopupMenu extends JPopupMenu {

    private final JMenuItem menuItemRemove = new JMenuItem("Entfernen");
    private final JMenuItem menuItemStop = new JMenuItem("Stop");
    private final JMenuItem menuItemStart = new JMenuItem("Start");
    private final JMenuItem menuItemRemoveDone = new JMenuItem("Alle fertigen löschen");
    private final JMenuItem menuItemRemoveAll = new JMenuItem("Alle löschen");

    public JTablePopupMenu(JTable table, DownloadsMvp.View view) {

        menuItemStop.setEnabled(false);
        menuItemStart.setEnabled(false);
        menuItemRemoveDone.setEnabled(false);
        menuItemRemoveAll.setEnabled(false);

        add(menuItemRemove);
        add(menuItemStop);
        add(menuItemStart);
        add(menuItemRemoveDone);
        add(menuItemRemoveAll);

        menuItemRemove.addActionListener((ActionEvent e) -> {
            final SFDLTableModel tableModel = (SFDLTableModel) table.getModel();
            final int sfdlIdAtRow = tableModel.getSFDLIdAtRow(table.getSelectedRow());
//            tableModel.removeSfdl(sfdlIdAtRow);
            view.removedSfdl(sfdlIdAtRow);
        });

        menuItemStop.addActionListener((ActionEvent e) -> {
            final SFDLTableModel model = (SFDLTableModel) table.getModel();
//            model.stopSfdl(model.getSFDLIdAtRow(table.getSelectedRow()));
        });
        
    }

}
