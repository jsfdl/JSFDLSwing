/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.nodomain.jsfdl.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

/**
 *
 * @author oli
 */
public class BottomPanel extends JPanel {

    private final javax.swing.JLabel jLabel1;
    private final javax.swing.JLabel jLabel2;
    private final javax.swing.JLabel jLabel3;
    private final javax.swing.JLabel jLabel4;
    private final javax.swing.JProgressBar jProgressBar1;

    public BottomPanel() {
        setBorder(new LineBorder(Color.black));
        setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        jProgressBar1 = new javax.swing.JProgressBar();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        jLabel1.setText("Status:");
        jLabel2.setText("Applikation gestartet.");
        jLabel3.setText("Speed:");
        jLabel4.setText("n/a");

        add(jLabel1);
        add(jLabel2);
        add(jLabel3);
        add(jLabel4);
    }

}
