package de.nodomain.jsfdl.gui;


import com.gitlab.jsfdl.jsfdl.bus.events.downloadchanged.DownloadInformation;
import com.gitlab.jsfdl.jsfdl.bus.events.sfdladded.SFDLFileAddedInformation;
import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadView;
import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadsMvp;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.util.ArrayList;

@DownloadView
public final class MyJTablePane extends JPanel implements DownloadsMvp.View {

    private final SFDLTableModel tableModel = new SFDLTableModel();
    private final MyJTable jtable = new MyJTable(tableModel);
    private final ArrayList<DownloadsMvp.SfdlFileRemovedListener> listeners = new ArrayList<>();


    public MyJTablePane() {
        setLayout(new BorderLayout());
        jtable.addMouseListener(new PopupMouseAdapter(jtable, this));
        setBorder(new LineBorder(Color.black));
        add(new JScrollPane(jtable), BorderLayout.CENTER);
    }

    @Override
    public void addedSFDLToDownload(SFDLFileAddedInformation file) {
        tableModel.addSfdlToDownload(file);
    }

    @Override
    public void allDownloadsStarted(java.util.List<Integer> started) {
        tableModel.allDownloadsStarted(started);
    }

    @Override
    public void allDownloadsAborted(java.util.List<Integer> aborted) {
        tableModel.allDownloadsAborted(aborted);
    }

    @Override
    public void downloadChangedForSFDL(DownloadInformation info) {
        tableModel.downloadChanged(info);
    }

    @Override
    public void removedSfdl(int sfdlFile) {
        listeners.forEach(sfdlFileRemovedListener -> sfdlFileRemovedListener.removedSfdl(sfdlFile));
        tableModel.removeSfdl(sfdlFile);
    }

    @Override
    public DownloadsMvp.DownloadChangedListener getDownloadChangedListenerFor(int id) {
        return tableModel;
    }

    @Override
    public void registerSfdlFileRemovedListener(DownloadsMvp.SfdlFileRemovedListener listener) {
        listeners.add(listener);
    }
}
