package de.nodomain.jsfdl.gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JTextField;

public class PasswordEnterDialog {

    final JDialog frame = new JDialog();
    private String password;
    private boolean passwordEntered;

    public PasswordEnterDialog() {
        
        frame.setModal(true);
        frame.setTitle("Password...");
        frame.setPreferredSize(new Dimension(200, 80));
        frame.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
        
        final JTextField passwordField = new JTextField();
        passwordField.setPreferredSize(new Dimension(100, 30));
        final JButton jButton = new JButton("Ok");

        jButton.addActionListener((ActionEvent e) -> {
            this.password = passwordField.getText();
            passwordEntered = true;
            frame.dispose();
        });

        frame.add(passwordField);
        frame.add(jButton);
        frame.setLocationRelativeTo(null);
        frame.pack();
        
        frame.getRootPane().setDefaultButton(jButton);
        
        passwordField.requestFocusInWindow();
    }

    public void setVisible(boolean visible) {
        frame.setVisible(visible);
    }

    String getPassword() {
        return password;
    }

    public boolean isPasswordEntered() {
        return passwordEntered;
    }

}
