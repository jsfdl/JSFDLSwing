
package de.nodomain.jsfdl.gui;

import javax.swing.*;

public class MyJTable extends JTable {

    public MyJTable(SFDLTableModel model) {
        super(model);
    }

    @Override
    public SFDLTableModel getModel() {
        return (SFDLTableModel) super.getModel(); 
    }
    
}
