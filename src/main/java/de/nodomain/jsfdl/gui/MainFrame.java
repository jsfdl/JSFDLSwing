package de.nodomain.jsfdl.gui;

import com.gitlab.jsfdl.jsfdl.mvp.dateiMenu.DateiMenuMvp;
import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadsMvp;
import com.gitlab.jsfdl.jsfdl.mvp.mainFrame.SetVisibleMethod;
import com.gitlab.jsfdl.jsfdl.mvp.startstop.StartStopMvp;

import javax.swing.*;
import java.awt.*;


public final class MainFrame extends javax.swing.JFrame implements SetVisibleMethod {

    @Override
    public void setVisible(DateiMenuMvp.View dateiMenuView, StartStopMvp.View startStopView, DownloadsMvp.View jtable) {
        Container pane = getContentPane();
        pane.setSize(500, 500);
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setJMenuBar((JMenuBar) dateiMenuView);

        pane.setLayout(new BorderLayout(10, 10));
        pane.add((JPanel) startStopView, BorderLayout.PAGE_START);
        pane.add((JPanel) jtable, BorderLayout.CENTER);
//        pane.add(bottomPanel, BorderLayout.PAGE_END);

        setMinimumSize(new Dimension(800, 500));
        setTitle("JSFDL Loader (alpha)");
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

}
